import './scss/bootstrap.scss';
import {Application} from './application';

export default class Main {

  constructor() {
    console.log('Special Capability. Webpack launched');

    new Application();
  }
}

let start = new Main();