
import {IDS} from './ids';
import {DOMUtil} from '../common/DOMUtil';
import {template} from './template';
import {Button} from '../buttons/Button';
import {Description} from '../description';


// todo: remove, use LiveStyle
export class WidgetStyle {
  static fontSize: number = 100;

  private static style: string = '';

  public static render () {
    let styleString: string = '';
    let styleElement = DOMUtil.query('#SPCDynamicStyle', document.head);
    if (!styleElement) {
      styleElement = DOMUtil.create('style', {'id': 'SPCDynamicStyle', 'type': 'text/css'});
      document.head.appendChild(styleElement);
    }

    styleString += `body * {font-size: ${WidgetStyle.fontSize}% !important;}`;

    styleElement.textContent = styleString;
  };

  public static remove () {
    const styleElement = DOMUtil.query('#SPCDynamicStyle', document.head);
    if (styleElement) {
      document.head.removeChild(styleElement);
    }
  }

}


export class Widget {

  private template: string = template;
  private node: Node;
  private description: Description;

  constructor (private buttons: Array <Button>) {
    this.node = DOMUtil.stringToHtml(this.template);
    this.description = new Description(this.node);

    this.initializeButtons();
  }

  public initializeButtons () {
    const menuHTMLElement = DOMUtil.create('ul');

    this.buttons.forEach((button: Button) => {
      button.setDescription(this.description);
      menuHTMLElement.appendChild(button.html());
    });

    DOMUtil.query('#' + IDS.BUTTONS, this.node).appendChild(menuHTMLElement);
  }



  public html () {
    return this.node;
  }

}
