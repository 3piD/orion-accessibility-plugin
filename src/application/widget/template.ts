import {IDS} from './ids';

const template = `
<div id="${IDS.WIDGET}">
    <div id="${IDS.ICON}">
        ICON
    </div>
    <div id="${IDS.DESCRIPTION}">
        Description
    </div>
    <div id="${IDS.HEADER}"></div>
    <div id="${IDS.BEFORE_BUTTONS}"></div>
    <div id="${IDS.BUTTONS}"></div>
    <div id="${IDS.AFTER_BUTTONS}"></div>
    <div id="${IDS.BOTTOM}"></div>
</div>
`;

export {template};