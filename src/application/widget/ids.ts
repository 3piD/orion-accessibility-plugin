
export const IDS = {
  ICON: 'SPCIcon',
  DESCRIPTION: 'SPCDescription',
  WIDGET: 'SPCWidget',
  BUTTONS: 'SPCButtons',
  BEFORE_BUTTONS: 'SPCBeforeButtons',
  AFTER_BUTTONS: 'SPCAfterButtons',
  HEADER: 'SPCHeader',
  BOTTOM: 'SPCBottom',
};
