import {DOMUtil} from '../common/DOMUtil';
import {IDS} from '../widget/ids';

export class Description {

  private widgetHTMLElement: HTMLElement;
  private descriptionHTMLElement: HTMLElement;

  constructor (private node: Node) {

    this.widgetHTMLElement = DOMUtil.query('#' + IDS.WIDGET, this.node);
    this.descriptionHTMLElement = DOMUtil.query('#' + IDS.DESCRIPTION, this.node);

    this.widgetHTMLElement.addEventListener('mouseover', (event) => {
      this.descriptionHTMLElement.style.display = 'block'
    });

    this.widgetHTMLElement.addEventListener('mouseout', (event) => {
      this.descriptionHTMLElement.style.display = 'none';
      this.descriptionHTMLElement.textContent = '';
    });
  }

  text (text: string) {
    this.descriptionHTMLElement.textContent = text;
  }

}
