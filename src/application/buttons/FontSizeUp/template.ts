
const data = {
  src: 'http://localhost/images/setting64.png',
  alt: 'Font Size Up',
  label: 'Font Size Up',
  title: 'Font Size Up title',
};

const template = `
<li class="spc-button" title="${data.title}">
    <div><img src="${data.src}" alt="${data.alt}"></div>
    <div>${data.label}</div>
</li>
`;

export {template};