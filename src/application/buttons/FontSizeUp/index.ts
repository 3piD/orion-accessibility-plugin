
import {template} from './template';
import {DOMUtil} from '../../common/DOMUtil';
import {Button} from '../Button';
import {WidgetStyle} from '../../widget';


export class FontSizeUp extends Button {

  public node: HTMLElement;
  public template: string = template;
  public active: boolean = false;

  constructor() {
    super();
    this.node = DOMUtil.query('li', DOMUtil.stringToHtml(this.template));

    this.onClick((eve) => {
      WidgetStyle.fontSize += 2;
      WidgetStyle.render();
    });

    this.onHover((eve) => {
      this.description.text('Font Size Up');
    });
  }

}
