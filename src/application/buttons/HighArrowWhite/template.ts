
const data = {
  src: 'http://localhost/images/setting64.png',
  alt: 'High Arrow alt',
  label: 'High Arrow',
  title: 'High Arrow title',
};

const template = `
<li class="SPCButton" title="${data.title}">
    <div><img src="${data.src}" alt="${data.alt}"></div>
    <div>${data.label}</div>
</li>
`;

export {template};