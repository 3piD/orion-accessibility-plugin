
import {template} from './template';
import {DOMUtil} from '../../common/DOMUtil';
import {Button} from '../Button';

export class HighArrowWhite extends Button {

  public node: HTMLElement;
  public template: string = template;
  public active: boolean = false;

  constructor() {
    super();
    this.node = DOMUtil.query('li', DOMUtil.stringToHtml(this.template));

    this.onClick((eve) => {
      this.active = !this.active;
      this.enableToggle();
    });

    this.onHover((eve) => {
      this.description.text('Expedita facere inventore iste libero officiis pariatu');
    });
  }

  enableToggle() {
    if (this.active) {
      document.body.classList.add('SPCHighArrowWhite');
    } else {
      document.body.classList.remove('SPCHighArrowWhite');
    }
  }

}
