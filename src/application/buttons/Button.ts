import {DOMUtil} from '../common/DOMUtil';
import {IButton} from './IButton';
import {Description} from '../description';

export const ButtonOptions = {};

export class Button implements IButton{

  public name: string;
  public template: string;
  public node: HTMLElement;
  public description: Description;

  constructor() {
  }

  setDescription(description: Description) {
    this.description = description;
  }

  onClick(callback: EventListenerOrEventListenerObject) {
    this.node.addEventListener('click', callback);
  }

  onHover(callback: EventListenerOrEventListenerObject) {
    this.node.addEventListener('mouseover', callback);
  }

  html(): HTMLElement {
    return this.node;
  }
}
