
const data = {
  src: 'http://localhost/images/setting64.png',
  alt: 'Increased Font alt',
  label: 'Increased Font',
  title: 'Increased Font title',
};

const template = `
<li class="spc-button" title="${data.title}">
    <div><img src="${data.src}" alt="${data.alt}"></div>
    <div>${data.label}</div>
</li>
`;

export {template};