
export interface IButton {
  name: string;
  template: string;
  node: HTMLElement;
  html: Function;
}
