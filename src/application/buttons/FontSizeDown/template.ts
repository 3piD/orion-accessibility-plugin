
const data = {
  src: 'http://localhost/images/setting64.png',
  alt: 'Font Size Down alt',
  label: 'Font Size Down',
  title: 'Font Size Down title',
};

const template = `
<li class="spc-button" title="${data.title}">
    <div><img src="${data.src}" alt="${data.alt}"></div>
    <div>${data.label}</div>
</li>
`;

export {template};