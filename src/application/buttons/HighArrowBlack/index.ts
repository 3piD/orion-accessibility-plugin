
import {template} from './template';
import {DOMUtil} from '../../common/DOMUtil';
import {Button} from '../Button';

export class HighArrowBlack extends Button {

  public node: HTMLElement;
  public template: string = template;
  public active: boolean = false;

  constructor() {
    super();
    this.node = DOMUtil.query('li', DOMUtil.stringToHtml(this.template));

    this.onClick((eve) => {
      this.active = !this.active;
      this.enableToggle();
    });

    this.onHover((eve) => {
      this.description.text('Ducimus expedita facere inventore iste libero officiis pariatu');
    });
  }

  enableToggle() {
    if (this.active) {
      document.body.classList.add('SPCHighArrowBlack');
    } else {
      document.body.classList.remove('SPCHighArrowBlack');
    }
  }

}
