import any = jasmine.any;

const DOMUtil = {

  /**
   * Create HTMLElement
   *
   * @param tagName
   * @param {{}} props
   * @param children
   * @returns {HTMLAnchorElement | HTMLAppletElement | HTMLAreaElement | HTMLAudioElement | HTMLBaseElement | HTMLBaseFontElement | HTMLQuoteElement | HTMLBodyElement | HTMLBRElement | HTMLButtonElement | HTMLCanvasElement | HTMLTableCaptionElement | HTMLTableColElement | HTMLDataElement | HTMLDataListElement | HTMLModElement | HTMLDirectoryElement | HTMLDivElement | HTMLDListElement | HTMLEmbedElement | HTMLFieldSetElement | HTMLFontElement | HTMLFormElement | HTMLFrameElement | HTMLFrameSetElement | HTMLHeadingElement | HTMLHeadElement | HTMLHRElement | HTMLHtmlElement | HTMLIFrameElement | HTMLImageElement | HTMLInputElement | HTMLUnknownElement | HTMLLabelElement | HTMLLegendElement | HTMLLIElement | HTMLLinkElement | HTMLPreElement | HTMLMapElement | HTMLMarqueeElement | HTMLMenuElement | HTMLMetaElement | HTMLMeterElement | HTMLObjectElement | HTMLOListElement | HTMLOptGroupElement | HTMLOptionElement | HTMLOutputElement | HTMLParagraphElement | HTMLParamElement | HTMLPictureElement | HTMLProgressElement | HTMLScriptElement | HTMLSelectElement | HTMLSourceElement | HTMLSpanElement | HTMLStyleElement | HTMLTableElement | HTMLTableSectionElement | HTMLTableDataCellElement | HTMLTemplateElement | HTMLTextAreaElement | HTMLTableHeaderCellElement | HTMLTimeElement | HTMLTitleElement | HTMLTableRowElement | HTMLTrackElement | HTMLUListElement | HTMLVideoElement | MSHTMLWebViewElement}
   */
  create: function (tagName: string, props = {}, ...children: any[]) {
    const element = document.createElement(tagName);

    Object.keys(props).forEach(
      key => element[key] = props[key]
    );

    children.forEach(child => {
      if (typeof child === 'string') {
        child = document.createTextNode(child);
      }

      if (child.nodeType === Node.TEXT_NODE ||
          child.nodeType === Node.ELEMENT_NODE ||
          child.nodeType === Node.DOCUMENT_FRAGMENT_NODE) {
        element.appendChild(child);
      }
    });
    return element;
  },

  /**
   * Convert String To Nodes.
   * Return NodeList with Nodes
   *
   * @param {string} html
   * @returns {NodeList}
   */
  stringToNodes: function (html: string) {
    return new DOMParser().parseFromString(html.trim(), 'text/html').body.childNodes
  },

  /**
   * Convert String To Node.
   * Create HTMLElement from String
   *
   * @param {string} html
   * @returns {Node | null | DocumentFragment}
   */
  stringToHtml: function (html: string) {
    let elem;
    const fragment = document.createDocumentFragment();
    const container = document.createElement('div');
    container.innerHTML = html;

    while (elem = container.firstChild) {
      fragment.appendChild(elem);
    }

    return fragment.childNodes.length === 1 ? fragment.firstChild : fragment;
  },

  /**
   * Apply `callback` function when DOM loaded
   *
   * @param {Function} callback
   */
  isLoaded: function (callback: Function) {
    if (document.querySelector('body')) {
      callback.call(null);
    } else {
      document.addEventListener('DOMContentLoaded', function () {
        callback.call(null)
      }, false);
    }
  },

  /**
   * Is availability Node
   *
   * @param value
   * @returns {any | boolean}
   */
  isNode: function (value: any) {
    return value && (value.nodeType === Node.TEXT_NODE ||
      value.nodeType === Node.ELEMENT_NODE ||
      value.nodeType === Node.DOCUMENT_FRAGMENT_NODE ||
      value.nodeType === Node.DOCUMENT_NODE)
  },

  /**
   * Find one element by selector
   *
   * @param selector
   * @param useFrom
   * @returns {Node | boolean}
   */
  query: function (selector: any, useFrom: any = null) {
    const elems = DOMUtil.queryAll(selector, useFrom);
    return elems && elems[0] ? elems[0] : false;
  },

  /**
   * Find elements by selector
   *
   * @param selector
   * @param useFrom
   * @returns {Node | null | Array}
   */
  queryAll: function (selector: any, useFrom: any = null) {
    const type = typeof useFrom;
    let getFrom = document;
    let elements: any[];

    if (DOMUtil.isNode(selector)) {
      return [selector];
    }

    else if (type === 'object' && DOMUtil.isNode(useFrom)) {
      getFrom = useFrom;
    }
    else if (type === 'string') {
      getFrom = document.querySelector(useFrom);
    }

    if (getFrom) {
      elements = [].slice.call(getFrom.querySelectorAll(selector));
    }

    return elements;
  },

  position: function (elem: any) {
    const param = {
      x: 0,
      y: 0,
      width: 0,
      height: 0,
      target: window
    };

    if (typeof elem === 'string') {
      elem = document.querySelector(elem);
    }

    if (elem === window || elem === document) {
      param.width = window.innerWidth;
      param.height = window.innerHeight;
    }
    else if (elem && elem.nodeType === Node.ELEMENT_NODE) {
      if (elem.getBoundingClientRect) {

        const rect = elem.getBoundingClientRect();
        const scrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop;
        const scrollLeft = window.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft;
        const clientTop = document.documentElement.clientTop || document.body.clientTop || 0;
        const clientLeft = document.documentElement.clientLeft || document.body.clientLeft || 0;

        param.y = Math.round(rect.top + scrollTop - clientTop);
        param.x = Math.round(rect.left + scrollLeft - clientLeft);
        param.width = elem.offsetWidth;
        param.height = elem.offsetHeight;
      } else {

        let top = 0;
        let left = 0;

        while (elem) {
          top  += parseInt(elem.offsetTop, 10);
          left += parseInt(elem.offsetLeft, 10);
          elem = elem.offsetParent;
        }

        param.y = top;
        param.x = left;
        param.width = elem.offsetWidth;
        param.height = elem.offsetHeight;
      }
      param.target = elem;
    }

    return param;
  },

};

export {DOMUtil};
