
import {DOMUtil} from './common/DOMUtil';
import {Widget} from './widget';
import {HighArrowBlack} from './buttons/HighArrowBlack';
import {HighArrowWhite} from './buttons/HighArrowWhite';
import {IncreasedFont} from './buttons/IncreasedFont';
import {FontSizeUp} from './buttons/FontSizeUp';
import {FontSizeDown} from './buttons/FontSizeDown';

// Base Application Controller
export class Application {

  constructor () {

    DOMUtil.isLoaded(() => {

      const widget = new Widget([
        new HighArrowBlack,
        new HighArrowWhite,
        new IncreasedFont,
        new FontSizeUp,
        new FontSizeDown,
      ]);

      document.body.appendChild(widget.html());

    });

  }

}
